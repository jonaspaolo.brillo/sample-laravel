import MyComponent from '../../../../slices/MySlice';

export default {
  title: 'slices/MySlice'
}


export const _Default = () => <MyComponent slice={{"variation":"default","name":"Default","slice_type":"my_slice","items":[],"primary":{"title":[{"type":"heading1","text":"Target efficient web services","spans":[]}],"description":[{"type":"paragraph","text":"Ullamco labore amet voluptate ea est esse pariatur officia et deserunt ipsum.","spans":[]}],"My-Image":{"dimensions":{"width":900,"height":500},"alt":"Placeholder image","copyright":null,"url":"https://images.unsplash.com/photo-1555169062-013468b47731?w=900&h=500&fit=crop"}},"id":"_Default"}} />
_Default.storyName = 'Default'
