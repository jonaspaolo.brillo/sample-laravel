<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\workerModel;
use Carbon\Carbon;

class workerController extends Controller
{
    public function getWorkersList(Request $request){
        $list=workerModel::get();
        return response([
            'status' => true,
            'data' => $list
        ], 200);
    }
    public function getWorkersListWithWhereClause(Request $request){
        $list=workerModel::where('id',$request->id)->get();
        if(count($list)>0){
            return response([
                'status' => true,
                'data' => $list
            ], 200);
        }
        else{
            return response([
                'status' => false,
                'data' => 'no data found'
            ], 400);
        }
    }
    public function getWorkersListWithSpecificData(Request $request){
        $list=workerModel::where('id',$request->id)->get();
        $table=[
            'full_name'=>$list[0]->first_name." ".$list[0]->last_name,
            'email'=>$list[0]->email,
        ];
        if(count($list)>0){
            return response([
                'status' => true,
                'data' => $table
            ], 200);
        }
        else{
            return response([
                'status' => false,
                'data' => 'no data found'
            ], 400);
        }
    }
    public function updateWorkersListWithSpecificData(Request $request){
       try{
            $list=workerModel::where('id',$request->id)
                    ->update(['email' => $request->email]);
            if($list){
                return response([
                    'status' => true,
                    'data' => 'data updated'
                    ], 200);
            }
            else{
                return response([
                    'status' => false,
                    'data' => 'data error'
                    ], 400);
            }
       }catch(Exception $e){
            return response([
                'status' => false,
                'data' => $e
                ], 400);
       }
    }
    public function insertWorkersListWithSpecificData(Request $request){
        try{
             $worker=new workerModel;
             $worker->first_name=$request->first_name;
             $worker->last_name=$request->last_name;
             $worker->email=$request->email;
             $worker->username=$request->username;
             $worker->password=md5($request->password);
             $worker->area_id=1;
             $worker->sys_update_date=Carbon::now();
             $worker->remarks='test';
             $worker->save();
             if($worker){
                 return response([
                     'status' => true,
                     'data' => 'data saved'
                     ], 200);
             }
             else{
                 return response([
                     'status' => false,
                     'data' => 'data error'
                     ], 400);
             }
        }catch(Exception $e){
             return response([
                 'status' => false,
                 'data' => $e
                 ], 400);
        }
     }
     public function deleteWorkersListWithSpecificData(Request $request){
        try{
            $list=workerModel::where('id',$request->id)
            ->delete();
             if($list){
                 return response([
                     'status' => true,
                     'data' => 'data deleted'
                     ], 200);
             }
             else{
                 return response([
                     'status' => false,
                     'data' => 'data error'
                     ], 400);
             }
        }catch(Exception $e){
             return response([
                 'status' => false,
                 'data' => $e
                 ], 400);
        }
     }
}
