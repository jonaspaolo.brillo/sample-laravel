<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class workerModel extends Model
{
    protected $connection = 'mysql';
    protected $table = 'workers';
    protected $primaryKey = 'id';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            "church_id",
            "first_name",
            "last_name",
            "email",
            "username",
            "password",
            "mobile",
            "address",
            "area_id",
            "birthdate",
            "facebook_handle",
            "ministry_id",
            "sec_ministry_id",
            "type",
            "status",
            "start_month",
            "start_year",
            "sys_create_id",
            "sys_create_date",
            "sys_update_id",
            "sys_update_date",
            "flag",
            "remarks",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
