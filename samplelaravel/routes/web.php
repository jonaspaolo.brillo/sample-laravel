<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/list-all', 'workerController@getWorkersList');
Route::get('/list-with-where', 'workerController@getWorkersListWithWhereClause');
Route::get('/list-with-specific-data', 'workerController@getWorkersListWithSpecificData');
Route::get('/insert-specific-data', 'workerController@insertWorkersListWithSpecificData');
Route::get('/update-specific-data', 'workerController@updateWorkersListWithSpecificData');
Route::get('/delete-specific-data', 'workerController@deleteWorkersListWithSpecificData');